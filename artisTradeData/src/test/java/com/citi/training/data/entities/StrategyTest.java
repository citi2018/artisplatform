package com.citi.training.data.entities;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StrategyTest {

	@Test
	public void strategy_CanInstantiate_ReturnNotNull() {
		Strategy strategy = new Strategy();
		strategy.setId(1);
		strategy.setName("AMZN STRAT 2");
		strategy.setTicker("AMZN");
		strategy.setEntrySize(100);
		strategy.setClosePercentage(5);
		assertThat(strategy,
				is(not(nullValue())));
	}

}
