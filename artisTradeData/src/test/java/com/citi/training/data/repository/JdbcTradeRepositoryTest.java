package com.citi.training.data.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.data.TestApplication;
import com.citi.training.data.entities.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=TestApplication.class)
public class JdbcTradeRepositoryTest {

    private static final Logger log = LoggerFactory.getLogger(JdbcTradeRepositoryTest.class);

    @Resource(name="JdbcTradeRepository")
    private TradeRepository tradeRepository;

    private Trade testTrade;
    @Test
    public void test_savingAndRetrieving() {
    }
    
   /* @Before
    public void createTrade() {
        String testStock = "AMZN";
        String testTradeType = "BUY";

        this.testTrade = new Trade(testStock, testTradeType);
    }*/

    // expects an empty trades table
   /* @Test
    public void test_savingAndRetrieving() {
        int orig_id = testTrade.getId();
        Trade newTrade = tradeRepository.saveTrade(testTrade);
        assertFalse(orig_id == newTrade.getId());
        assertTrue(newTrade == testTrade);

        Trade retTrade = tradeRepository.getTradeById(newTrade.getId());
        log.debug("GetById returned: " + retTrade);
        assertTrue(retTrade.equals(newTrade));


        // add another trade
        testTrade.setId(-1);
        retTrade = tradeRepository.saveTrade(testTrade);

        List<Trade> trades = tradeRepository.getAllTrades();
        assertTrue(trades.size() >= 2);

        trades = tradeRepository.getTradesByStock(testTrade.getStock());
        assertTrue(trades.size() >= 2);

        String testStock = "UUUU";
        trades.get(0).setStock(testStock);
        tradeRepository.saveTrade(trades.get(0));
        retTrade = tradeRepository.getTradeById(trades.get(0).getId());
        assertTrue(retTrade.getStock().equals(testStock));

        Trade.TradeState testState = Trade.TradeState.FILLED;
        trades.get(0).setState(testState);
        tradeRepository.saveTrade(trades.get(0));
        trades = tradeRepository.getTradesByState(testState);
        assertTrue(trades.get(0).getState() == testState);
    }*/
}
