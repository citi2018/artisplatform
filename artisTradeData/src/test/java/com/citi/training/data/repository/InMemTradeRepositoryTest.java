package com.citi.training.data.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.citi.training.data.entities.Trade;

public class InMemTradeRepositoryTest {

    private TradeRepository tradeRepository = new InMemTradeRepository();
    private Trade testTrade; 

    @Before
    public void createTrade() {
        this.testTrade = new Trade();
    }

    @Test
    public void test_savingAndRetrieving() {
        assertTrue(tradeRepository.getAllTrades().size() == 0);

        int orig_id = testTrade.getId();
        Trade newTrade = tradeRepository.saveTrade(testTrade);
        assertFalse(orig_id == newTrade.getId());
        assertTrue(newTrade == testTrade);

        List<Trade> amznTrades = tradeRepository.getTradesByStock("AMZN");
        assertTrue(amznTrades.get(0) == newTrade);

        Trade foundById = tradeRepository.getTradeById(newTrade.getId());
        assertTrue(foundById == newTrade);

        List<Trade> foundByState = tradeRepository.getTradesByState(Trade.TradeState.INIT);
        assertTrue(foundByState.size() == 1);
        assertTrue(foundByState.get(0).getTradeState() == Trade.TradeState.INIT);
    }

}
