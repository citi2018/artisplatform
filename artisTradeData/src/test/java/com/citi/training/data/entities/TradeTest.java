package com.citi.training.data.entities;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeTest {
	@Test
	public void trade_CanInstantiate_ReturnNotNull() {
		Trade trade = new Trade();
		trade.setId(1);
		trade.setPrice(127);
		trade.setSize(1100);
		assertThat(trade,
				is(not(nullValue())));
	}
}
