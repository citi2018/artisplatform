package com.citi.training.data.entities;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StrategiesTest {

	@Test
	public void strategies_CanInstantiate_ReturnNotNull() {
		Strategies strategies = new Strategies();
		strategies.setId(1);
		strategies.setEntrySize(100);
		strategies.setIsActive(true);
		assertThat(strategies,
				is(not(nullValue())));
	}

}
