package com.citi.training.data.entities;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DoubleAverageTest {

	@Test
	public void doubleAverage_CanInstantiate_ReturnNotNull() {
		DoubleAverage doubleAverage = new DoubleAverage();
		doubleAverage.setId(1);
		doubleAverage.setName("AMZN Double Average");
		doubleAverage.setTicker("AMZN");
		assertThat(doubleAverage,
				is(not(nullValue())));
	}

}
