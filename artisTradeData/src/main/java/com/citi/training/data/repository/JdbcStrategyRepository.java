package com.citi.training.data.repository;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.data.entities.Strategies;
import com.citi.training.data.entities.Strategy;
import com.citi.training.data.entities.Trade;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository("JdbcStrategyRepository")
public class JdbcStrategyRepository implements StrategyRepository {

	private static final Logger log = LoggerFactory.getLogger(JdbcStrategyRepository.class);
	
	@Autowired
	private JdbcTemplate tpl;

	@Value("${trade.table.name:stategies}")
	private String tableName = "strategies";

	private final String insertSQL = "INSERT INTO " + tableName + " (id, isActive, name, closePercentage, entrySize, ticker, profitValue) values (:id, :isActive, :name, :closePercentage, :entrySize, :ticker, :profitValue)";

	private final String updateSQL = "UPDATE " + tableName + " SET stock=:stock, "
			+ "price=:price, size=:size, lastStateChange=:lastStateChange, tradeType=:tradeType, state=:state "
			+ "WHERE id=:id";
	
	private final String selectAll = "SELECT * FROM " + tableName;
	private final String selectByIdSQL = "SELECT * FROM " + tableName + " WHERE id=?";
	private final String selectByStockSQL = "SELECT * FROM " + tableName + " WHERE stock=?";
	private final String selectByStateSQL = "SELECT * FROM " + tableName + " WHERE state=?";
	private final String selectAllSQL = "SELECT * FROM " + tableName;
	private final String selectActive = "SELECT * FROM " + tableName + " WHERE isActive=true";
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	// returns the object given
	// if id < -1 then it will be inserted, otherwise updated
	public Strategy saveStrategy(Strategy strategy) {
		BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(strategy);

		// use string value of tradeType and state enums
		namedParameters.registerSqlType("id", Types.INTEGER);
		namedParameters.registerSqlType("isActive", Types.TINYINT);
		namedParameters.registerSqlType("name", Types.VARCHAR);
		namedParameters.registerSqlType("closePercentage", Types.INTEGER);
		namedParameters.registerSqlType("entrySize", Types.INTEGER);
		namedParameters.registerSqlType("ticker", Types.VARCHAR);
		namedParameters.registerSqlType("profitValue", Types.DOUBLE);

		//log.debug("Inserting strategy: " + strategy);

		KeyHolder keyHolder = new GeneratedKeyHolder();

		namedParameterJdbcTemplate.update(insertSQL, namedParameters, keyHolder);
		strategy.setId(keyHolder.getKey().intValue());

		log.info("JdbcRepo returning trade: " + strategy);
		return strategy;
	}
	

	public List<Strategies> getAllStrategies() {
		log.debug("JdbcStrategiesRepo getAll");
		List<Strategies> strategies = jdbcTemplate.query(selectActive,
				new BeanPropertyRowMapper<Strategies>(Strategies.class));

		log.debug("Query for all returned list of size: " + strategies.size());
		return strategies;
	}
	
	public List<Strategies> getActiveStrategy() {
		log.debug("JdbcStrategiesRepo getAll");
		List<Strategies> strategies = jdbcTemplate.query(selectAllSQL,
				new BeanPropertyRowMapper<Strategies>(Strategies.class));

		log.debug("Query for all returned list of size: " + strategies.size());
		return strategies;
	}

	@Override
	public List<Strategies> listStrategy() {
		log.debug("JdbcTradeRepo get all strategies");
		List<Strategies> strategy = jdbcTemplate.query(selectAll, new BeanPropertyRowMapper<Strategies>(Strategies.class));

		return strategy;
	}
}
