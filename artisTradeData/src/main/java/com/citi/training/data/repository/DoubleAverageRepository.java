package com.citi.training.data.repository;

import java.util.List;

import com.citi.training.data.entities.DoubleAverage;

public interface DoubleAverageRepository {
    List<DoubleAverage> findByIsActive(boolean isActive);
    List<DoubleAverage> getAll();
}


