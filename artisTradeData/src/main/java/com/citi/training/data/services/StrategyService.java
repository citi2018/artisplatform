package com.citi.training.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.citi.training.data.entities.Strategies;
import com.citi.training.data.entities.Strategy;
import com.citi.training.data.entities.Trade;
import com.citi.training.data.repository.JdbcStrategyRepository;
import com.citi.training.data.repository.StrategyRepository;

@Service
public class StrategyService {

    @Autowired
    private JdbcStrategyRepository JdbcstrategyRepository;
    
    @Autowired
    private StrategyRepository strategyRepository;


    public Strategy saveStrategy(Strategy newStrategy) {
      return JdbcstrategyRepository.saveStrategy(newStrategy);
  }
    
    public Strategy createStrategy(int id, boolean isActive, String name, int close, int EntrySize, String Ticker, int ProfitValue ) {
       return strategyRepository.saveStrategy(new Strategy());
    }

	public List<Strategies> listStrategy() {
		return JdbcstrategyRepository.listStrategy();
	}
	
	public List<Strategies> getAllStrategies() {
		return JdbcstrategyRepository.getAllStrategies();
	}
	
	public List<Strategies> getActiveStrategies() {
		return JdbcstrategyRepository.getActiveStrategy();
	}

	
   
}

