package com.citi.training.data.services;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.data.entities.Trade;
import com.citi.training.data.entities.Trade.TradeState;
import com.citi.training.data.entities.Trade.TradeType;
import com.citi.training.data.repository.TradeRepository;

@Service
public class TradeService {

    @Autowired
    private TradeRepository tradeRepository;

    public Trade saveTrade(Trade newTrade) {
        return tradeRepository.saveTrade(newTrade);
    }

    public Trade createTrade(String stock, float price, int size, LocalDateTime lastStateChange,
			TradeType TradeType, TradeState TradeState, int strategyId) {
        return tradeRepository.saveTrade(new Trade(stock, price, size, lastStateChange, TradeType, TradeState, strategyId));
    }

    public Trade getTradeById(int id) {
        return tradeRepository.getTradeById(id);
    }

    public List<Trade> getTradesByStock(String stock) {
        return tradeRepository.getTradesByStock(stock);
    }

    public List<Trade> getTradesByState(Trade.TradeState state) {
        return tradeRepository.getTradesByState(state);
    }
    
    public Trade getLatestTrades() {
        return tradeRepository.getLatestTrade();
    }
    public List<Trade> getTradesByStrategyID(Integer strategy) {
        return tradeRepository.getTradesByStrategyID(strategy);
    }

    public List<Trade> getAllTrades() {
        return tradeRepository.getAllTrades();
    }
}
