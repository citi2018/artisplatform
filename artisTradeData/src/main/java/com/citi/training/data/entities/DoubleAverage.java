package com.citi.training.data.entities;

public class DoubleAverage extends Strategies {

    public DoubleAverage() {
    }
    
    private Integer longTime;
    private Integer shortTime;
    private Integer id;
    
    public Integer getLongTime() {
        return longTime;
    }

    public void setLongTime(Integer longTime) {
        this.longTime = longTime;
    }

    public Integer getShortTime() {
        return shortTime;
    }

    public void setShortTime(Integer shortTime) {
        this.shortTime = shortTime;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
    
 
}
