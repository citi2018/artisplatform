package com.citi.training.data.repository;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.data.entities.Trade;
import com.citi.training.data.entities.Trade.TradeState;

@Repository("JdbcTradeRepository")
public class JdbcTradeRepository implements TradeRepository {

    private static final Logger log = LoggerFactory.getLogger(JdbcTradeRepository.class);

    @Value("${trade.table.name:trades}")
    private String tableName = "trades";

    private final String insertSQL = "INSERT INTO " + tableName + " (stock, price, size, lastStateChange, " +
                                     "TradeType, TradeState, strategyId) values (:stock, :price, :size, :lastStateChange, " +
                                     ":TradeType, :TradeState, :strategyId)";

    private final String updateSQL = "UPDATE " + tableName + " SET stock=:stock, " +
                                     "price=:price, size=:size, lastStateChange=:lastStateChange, TradeType=:TradeType, TradeState=:TradeState, strategyId=:strategyId " +
                                     "WHERE id=:id";

    private final String selectByIdSQL = "SELECT * FROM " + tableName + " WHERE id=?";
    private final String selectByStockSQL = "SELECT * FROM " + tableName + " WHERE stock=?";
    private final String selectByStateSQL = "SELECT * FROM " + tableName + " ORDER BY id DESC LIMIT 1";
    private final String selectByStrategyID = "SELECT * FROM " + tableName + " WHERE strategyId=?";
    private final String selectAllSQL = "SELECT * FROM " + tableName;
    
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    // returns the object given
    // if id < -1 then it will be inserted, otherwise updated
    public Trade saveTrade(Trade trade) {
        BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(trade);

        // use string value of tradeType and state enums
//		namedParameters.registerSqlType("id", Types.INTEGER);
//        namedParameters.registerSqlType("stock", Types.VARCHAR);
//        namedParameters.registerSqlType("price", Types.FLOAT);
//        namedParameters.registerSqlType("size", Types.INTEGER);
//        namedParameters.registerSqlType("lastStateChange", Types.VARCHAR);
        namedParameters.registerSqlType("TradeType", Types.VARCHAR);
        namedParameters.registerSqlType("TradeState", Types.VARCHAR);
//        namedParameters.registerSqlType("strategyId", Types.VARCHAR);

        if (trade.getId() < 0) {
            // insert
            log.debug("Inserting trade: " + trade);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(insertSQL, namedParameters, keyHolder);
            trade.setId(keyHolder.getKey().intValue());
        } else {
            log.debug("Updating trade: " + trade);
            namedParameterJdbcTemplate.update(updateSQL, namedParameters);
        }
        log.info("JdbcRepo returning trade: " + trade);
        return trade;
    }

    public Trade getTradeById(int id) {
        log.debug("JdbcTradeRepo getById: " + id);
        List<Trade> trades = jdbcTemplate.query(selectByIdSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class),
                                                id);

        log.debug("Query for id <" + id + "> returned list: " + trades);
        return trades.get(0);
    }

    public List<Trade> getTradesByStock(String stock) {
        log.debug("JdbcTradeRepo getByStock: " + stock);
        List<Trade> trades = jdbcTemplate.query(selectByStockSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class),
                                                stock);

        log.debug("Query for stock <" + stock + "> returned list of size: " + trades.size());
        return trades;
    }

    public Trade getLatestTrade() {
  
        List<Trade> trades = jdbcTemplate.query(selectByStateSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class));
        return trades.get(0);
    }
    
    public List<Trade> getTradesByStrategyID(int strategy) {
        log.debug("JdbcTradeRepo getByStrategy: " + strategy);
        List<Trade> trades = jdbcTemplate.query(selectByStrategyID,
                                                new BeanPropertyRowMapper<Trade>(Trade.class), strategy);

        log.debug("Query for strategy <" + strategy + "> returned list of size: " + trades.size());
        return trades;
    }

    public List<Trade> getAllTrades() {
        log.debug("JdbcTradeRepo getAll");
        List<Trade> trades = jdbcTemplate.query(selectAllSQL,
                                                new BeanPropertyRowMapper<Trade>(Trade.class));

        log.debug("Query for all returned list of size: " + trades.size());
        return trades;
    }

	@Override
	public List<Trade> getTradesByState(TradeState state) {
		// TODO Auto-generated method stub
		return null;
	}
}
