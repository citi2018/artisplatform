package com.citi.training.data.entities;

import java.io.StringReader;
import java.time.LocalDateTime;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StockPriceRecord {
    private static final Logger log = LoggerFactory.getLogger(StockPriceRecord.class);

    // Static objects for marshalling to/from XML
    private static JAXBContext jaxbContext = null;
    private static Unmarshaller unmarshaller = null;
    private static Marshaller marshaller = null;
    
    static {
        try {
            jaxbContext = JAXBContext.newInstance(StockPriceRecord.class);
            unmarshaller = jaxbContext.createUnmarshaller();
            marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (JAXBException e) {
            log.error("Unable to create Trade JAXB elements");
        }
    }
    public static StockPriceRecord fromXml(String xmlString) {
        try {
            return (StockPriceRecord)unmarshaller.unmarshal(new StringReader(xmlString));
        } catch (JAXBException e) {
            log.error("Unable to unmarshal record: " + xmlString);
            log.error("JAXBException: " + e);
        }
        return null;
    }
    public StockPriceRecord() {
    }
    public StockPriceRecord(String ticker, Float price){
    	this.ticker =ticker;
    	this.price = price;
    }
    
	private int id;
    private String ticker;
    private Float price;
    
    @XmlTransient
    private LocalDateTime timeInspected = LocalDateTime.now();

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof StockPriceRecord)) {
            return false;
        }
        StockPriceRecord s = (StockPriceRecord) o;
        return this.id == s.id;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
    	this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
    
    public LocalDateTime gettimeInspected() {
        return timeInspected;
    }

    public void settimeInspected(LocalDateTime timeInspected) {
        this.timeInspected = timeInspected;
    }
    // methods for mashalling/unmarshalling timeInspected
    @XmlElement(name="whenAsDate")
    public String gettimeInspectedXml() {
        return timeInspected.toString();
    }

    public void settimeInspectedXml(String timeInspected) {
        this.timeInspected = LocalDateTime.parse(timeInspected);
    }

	@Override
	public String toString() {
		return "StockPriceRecord [id=" + id + ", ticker=" + ticker + ", price=" + price + "]";
	}
    
	
}
