package com.citi.training.data.services;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.data.entities.StockPriceRecord;

import com.citi.training.data.entities.Trade;
import com.citi.training.data.repository.JdbcStockPriceRecordRepository;
import com.citi.training.data.repository.StockPriceRecordRepository;

@Service
public class StockPriceHistoryService {
	@Autowired
	JdbcStockPriceRecordRepository jdbcStockPrice;
	@Autowired
	StockPriceRecordRepository stockPrice;

	public List<StockPriceRecord> getAllRecords(){
		return stockPrice.getAllRecords();
	}

	public StockPriceRecord saveRecord(StockPriceRecord newRecord) {
		return stockPrice.saveRecord(newRecord);
	}

	public StockPriceRecord createStockPrice(String ticker, Float price ) {
        return stockPrice.saveRecord(new StockPriceRecord(ticker, price));
    }

	public float getLongAverage(String ticker) throws ParseException{
		return jdbcStockPrice.getLongAverage(ticker);
	}

	public float getShortAverage(String ticker) throws ParseException{
		return jdbcStockPrice.getShortAverage(ticker);
	}
	/*
	 * needs a list all and a save  functon called in the business layer and then called in the engine layer
	 */
}
