package com.citi.training.data.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.citi.training.data.entities.Trade;

// This line is just stopping a bean being created for now
@Profile("inmemrepos")
@Repository
public class InMemTradeRepository implements TradeRepository {

    private static final Logger log = LoggerFactory.getLogger(InMemTradeRepository.class);

    private final HashMap<Integer, Trade> trades = new HashMap<Integer, Trade>();

    private static int next_id = 0;

    public Trade saveTrade(Trade trade) {
        if (!trades.containsKey(trade.getId())) {
            trade.setId(next_id++);

            log.debug("Inserting trade: " + trade);
            trades.put(trade.getId(), trade);
        } else {
            log.debug("Updating trade: " + trade);
            trades.replace(trade.getId(), trade);
        }
        return trade;
    }

    public Trade getTradeById(int id) {
        log.debug("InMemTradeRepo getById: " + id);
        log.debug("Trades: " + trades.get(0));

        return trades.get(id);
    }

    public List<Trade> getTradesByStock(String stock) {
        ArrayList<Trade> filteredTrades = new ArrayList<Trade>();
        for (Trade trade : trades.values()) {
            if (trade.getStock().equals(stock)) {
                filteredTrades.add(trade);
            }
        }
        log.debug("Search for stock: " + stock + ", found: " + filteredTrades.size());
        return filteredTrades;
    }

    public List<Trade> getTradesByState(Trade.TradeState state) {
        ArrayList<Trade> filteredTrades = new ArrayList<Trade>();
        for (Trade trade : trades.values()) {
            if (trade.getTradeState() == state) {
                filteredTrades.add(trade);
            }
        }
        log.debug("Search for state: " + state + ", found: " + filteredTrades.size());
        return filteredTrades;
    }

    public List<Trade> getAllTrades() {
        return new ArrayList<Trade>(this.trades.values());
    }

	@Override
	public List<Trade> getTradesByStrategyID(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Trade getLatestTrade() {
		// TODO Auto-generated method stub
		return null;
	}
}
