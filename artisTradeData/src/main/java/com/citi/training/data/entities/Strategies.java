package com.citi.training.data.entities;

import com.fasterxml.jackson.annotation.*;

import java.util.List;

public class Strategies {

	private Integer id;
	private Boolean isActive = true;
	private String name;
	private Integer closePercentage;
	private Integer entrySize;
	private String ticker;
	private Double profitValue;
	
	public Strategies() {
		
	}
	@JsonIgnore
	private List<Trade> trades;

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof Strategies)) {
			return false;
		}
		Strategies s = (Strategies) o;
		return this.getId() == s.getId();
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean active) {
		isActive = active;
	}

	public Integer getClosePercentage() {
		return closePercentage;
	}

	public void setClosePercentage(Integer closePercentage) {
		this.closePercentage = closePercentage;
	}

	public Integer getEntrySize() {
		return entrySize;
	}

	public void setEntrySize(Integer entrySize) {
		this.entrySize = entrySize;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getProfitValue() {
		return profitValue;
	}

	public void setProfitValue(Double profitValue) {
		this.profitValue = profitValue;
	}

	public void setTrades(List<Trade> trades) {
		this.trades = trades;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public List<Trade> getTrades() {
		return trades;
	}
}
