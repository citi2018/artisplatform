package com.citi.training.data.entities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Holdings {
	private static final Logger log = LoggerFactory.getLogger(Trade.class);
	private String stockName;
	private int holdings;
	private double cashBalance;
    private int strategyId =0;

	
	public Holdings() {}//default constructor
	public Holdings(String stockName, int holdings, double cashBalance, int strategyId) {
		this.stockName = stockName;
		this.holdings = holdings;
		this.cashBalance = cashBalance;
		this.strategyId = strategyId;
	}//constructor with all variables
	
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public int getHoldings() {
		return holdings;
	}
	public void setHoldings(int holdings) {
		this.holdings = holdings;
	}
	public double getCashBalance() {
		return cashBalance;
	}
	public void setCashBalance(double cashBalance) {
		this.cashBalance = cashBalance;
	}
	
	public int getStrategyId() {
		return strategyId;
	}
	public void setStrategyId(int strategyId) {
		this.strategyId = strategyId;
	}
	@Override
	public String toString() {
		return "Holdings [stockName=" + stockName + ", holdings=" + holdings + ", cashBalance=" + cashBalance
				+ ", strategyId=" + strategyId + "]";
	}
	
	
	
}
