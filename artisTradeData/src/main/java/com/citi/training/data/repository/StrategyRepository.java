package com.citi.training.data.repository;

import java.util.List;

import com.citi.training.data.entities.Strategies;
import com.citi.training.data.entities.Strategy;

public interface StrategyRepository {

    public Strategy saveStrategy(Strategy Strategy);

	public List<Strategies> listStrategy();

}
