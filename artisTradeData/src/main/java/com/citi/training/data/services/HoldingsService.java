package com.citi.training.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citi.training.data.entities.Holdings;
import com.citi.training.data.entities.Trade;
import com.citi.training.data.repository.HoldingsRepository;
import com.citi.training.data.repository.JdbcHoldingsRepository;

@Service
public class HoldingsService {
	@Autowired
	JdbcHoldingsRepository holdingsRepository;
	
	public List<Holdings> getAllHoldings(){
		return holdingsRepository.getAllHoldings();
	}
	public List<Holdings> getHoldingsByStrategyID(Integer strategy) {
        return holdingsRepository.getHoldingsByStrategy(strategy);
    }
}
