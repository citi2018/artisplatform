package com.citi.training.data.repository;

import java.sql.Types;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.data.entities.StockPriceRecord;

@Repository("JdbcStockPriceRecordRepository")
public class JdbcStockPriceRecordRepository implements StockPriceRecordRepository {
	private static final Logger log = LoggerFactory.getLogger(JdbcStockPriceRecordRepository.class);
	@Autowired
    JdbcTemplate jdbcTemplate;
	 @Autowired
	    NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	 
	  private String tableName = "stockpricerecords";

//    private String tableNameTwo = "holdings";
    private final String selectAllSQL = "SELECT * FROM " + tableName;
    private final String insertSQL = "INSERT INTO " + tableName + "(ticker, price)"
    		+ "VALUES(:ticker, :price)";
    
    public float getLongAverage(String ticker) throws ParseException{
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse("2018-08-28");
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        final String selectLongAvg = 
        		"SELECT AVG(price) FROM (SELECT * FROM stockpricerecords WHERE ticker=? ORDER BY id DESC LIMIT 1000) AS T";
        float LongAverage = jdbcTemplate.queryForObject(selectLongAvg, Float.class, ticker);
 
        return LongAverage;
    	 
    }
    
    public float getShortAverage(String ticker) throws ParseException{
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse("2018-08-28");
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());
        final String selectShortAvg = 
        		"SELECT AVG(price) FROM (SELECT * FROM stockpricerecords WHERE ticker=? ORDER BY id DESC LIMIT 10) AS T";
        float ShortAverage = jdbcTemplate.queryForObject(selectShortAvg, Float.class, ticker);
 
        return ShortAverage;
    	 
    }

    public List<StockPriceRecord> getAllRecords(){
    	log.debug("JdbcHoldingsRepo getAll");
    	List<StockPriceRecord> records = jdbcTemplate.query(selectAllSQL,
    			new BeanPropertyRowMapper<StockPriceRecord>(StockPriceRecord.class));
    	log.debug("Query for all holdings returned list of size: " + records.size());
    	return records; 
    }//getAllRecords
    
    public StockPriceRecord saveRecord(StockPriceRecord stockPrice) {
        BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(stockPrice);
            log.debug("Inserting Stock Price: " + stockPrice);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(insertSQL,namedParameters, keyHolder);
            stockPrice.setId(keyHolder.getKey().intValue());
        log.info("JdbcRepo returning stock price: " + stockPrice);
        return stockPrice;
    }
}
