package com.citi.training.data.repository;

import java.util.List;

import com.citi.training.data.entities.StockPriceRecord;

public interface StockPriceRecordRepository {
	
	public StockPriceRecord saveRecord(StockPriceRecord stockRecord);
	
	public List<StockPriceRecord> getAllRecords();
}
