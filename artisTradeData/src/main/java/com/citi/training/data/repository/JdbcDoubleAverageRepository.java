package com.citi.training.data.repository;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.citi.training.data.entities.DoubleAverage;


@Repository("JdbcDoubleAverageRepository")
public class JdbcDoubleAverageRepository implements DoubleAverageRepository {
	private static final Logger log = LoggerFactory.getLogger(JdbcTradeRepository.class);
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    
    @Value("${holdings.table.name:trades}")
    private String tableName = "twomovingaveragesstrategies";
    private final String selectAllSQL = "SELECT * FROM " + tableName;
    private final String insertSQL = "INSERT INTO " + tableName + " (Id, LongTime, ShortTime) "
    		+ "values (:Id, :LongTime, :ShortTIme)";
    
    
    public List<DoubleAverage> getAll(){
    	log.debug("JdbcHoldingsRepo getAll");
    	List<DoubleAverage> holdings = jdbcTemplate.query(selectAllSQL,
    			new BeanPropertyRowMapper<DoubleAverage>(DoubleAverage.class));
    	
    	log.debug("Query for all holdings returned list of size: " + holdings.size());
    	return holdings;
    }
    
    public DoubleAverage saveDoubleAverage(DoubleAverage doubleAverage) {
    	BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(doubleAverage);
    	namedParameters.registerSqlType("Id", Types.INTEGER);
    	namedParameters.registerSqlType("LongTime", Types.INTEGER);
    	namedParameters.registerSqlType("ShortTime", Types.INTEGER);
    	
    	if(doubleAverage.getId() < 0) {
    		log.debug("Inserting double average: " + doubleAverage);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(insertSQL,namedParameters, keyHolder);
            doubleAverage.setId(keyHolder.getKey().intValue());
    	}else {
    		log.debug("Updating double average: " + doubleAverage);
    		namedParameterJdbcTemplate.update(insertSQL, namedParameters);
    	}
    	return doubleAverage;
    } 
	@Override
	public List<DoubleAverage> findByIsActive(boolean isActive) {
		// TODO Auto-generated method stub
		return null;
	}
}
