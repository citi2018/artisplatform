package com.citi.training.data.repository;

import java.util.List;

import com.citi.training.data.entities.Holdings;

public interface HoldingsRepository {
	public List<Holdings> getAllHoldings();
}
