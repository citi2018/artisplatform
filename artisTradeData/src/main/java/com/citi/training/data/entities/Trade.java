package com.citi.training.data.entities;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringReader;
import java.io.StringWriter;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@XmlRootElement(name = "trade")
@XmlAccessorType(XmlAccessType.FIELD)
// Ignore properties we created for Xml marshalling
@JsonIgnoreProperties({"tradeTypeXml", "stateXml", "lastStateChangeXml"})
public class Trade {
    private static final Logger log = LoggerFactory.getLogger(Trade.class);

    // Static objects for marshalling to/from XML
    private static JAXBContext jaxbContext = null;
    private static Unmarshaller unmarshaller = null;
    private static Marshaller marshaller = null;

    static {
        try {
            jaxbContext = JAXBContext.newInstance(Trade.class);
            unmarshaller = jaxbContext.createUnmarshaller();
            marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (JAXBException e) {
            log.error("Unable to create Trade JAXB elements");
        }
    }

    public enum TradeType {
        BUY ("true"),
        SELL ("false");

        private final String xmlString;

        private TradeType(String xmlString) {
            this.xmlString = xmlString;
        }

        public String getXmlString() {
            return this.xmlString;
        }

        public static TradeType fromXmlString(String xmlValue) {
            switch(xmlValue.toLowerCase()) {
                case "true" : return TradeType.BUY;
                case "false" : return TradeType.SELL;
                default: throw new IllegalArgumentException(xmlValue);
            }
        }
    }

    public enum TradeState {
        INIT,
        WAITING_FOR_REPLY,
        FILLED,
        PARTIALLY_FILLED,
        CANCELED,
        DONE_FOR_DAY,
        REJECTED;
    }

    private int id = -1;
    private String stock;
    private float price;
    private int size;
    private int strategyId =0;

    // ignore these fields in xml, we will use the getters/setters for xml
    @XmlTransient
    private LocalDateTime lastStateChange = LocalDateTime.now();

    @XmlTransient
    private TradeType tradeType;

    @XmlTransient
    private TradeState tradeState;


    public Trade(String stock, float price, int size, LocalDateTime lastStateChange,
			TradeType tradeType, TradeState tradeState, int strategyId) {
		this.stock = stock;
		this.price = price;
		this.size = size;
		this.strategyId = strategyId;
		this.lastStateChange = lastStateChange;
		this.tradeType = tradeType;
		this.tradeState = tradeState;
	}

    public Trade() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public TradeType getTradeType() {
        return tradeType;
    }

    public void setTradeType(TradeType tradeType) {
        this.tradeType = tradeType;
    }

    // methods for mashalling/unmarshalling tradeType
    @XmlElement(name="buy")
    public String getTradeTypeXml() {
        return this.tradeType.getXmlString();
    }

    public void setTradeTypeXml(String tradeTypeStr) {
        this.tradeType = TradeType.fromXmlString(tradeTypeStr);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public LocalDateTime getLastStateChange() {
        return lastStateChange;
    }

    public void setLastStateChange(LocalDateTime lastStateChange) {
        this.lastStateChange = lastStateChange;
    }

    // methods for mashalling/unmarshalling lastStateChange
    @XmlElement(name="whenAsDate")
    public String getLastStateChangeXml() {
        return lastStateChange.toString();
    }

    public void setLastStateChangeXml(String lastStateChange) {
        this.lastStateChange = LocalDateTime.parse(lastStateChange);
    }

    public TradeState getTradeState() {
        return tradeState;
    }

    public void setTradeState(TradeState tradeState) {
        this.tradeState = tradeState;
    }

    // to be called when state is changing (setState used for obj creation etc..)
    public void stateChange(TradeState newState) {
        this.tradeState = newState;
        this.lastStateChange = LocalDateTime.now();
    }

    // methods for mashalling/unmarshalling state
    @XmlElement(name="result")
    public TradeState getStateXml() {
        // don't include state when marshalling obj=>xml
        return null;
    }

    public void setStateXml(TradeState state) {
        stateChange(state);
    }

    @Override
    public String toString() {
        return "Trade [id=" + id + ", stock=" + stock +
               ", TradeType=" + tradeType + ", price=" + price +
               ", size=" + size + ", TradeState=" + tradeState +
               ", lastStateChange=" + lastStateChange + 
               ", strategyId" + strategyId + "]";
    }

    @Override
    public boolean equals(Object trade) {
        if (trade == this) {
            return true;
        }

        // This implementation allows ids to be different
        return trade != null && (trade instanceof Trade) &&
               getStock().equals(((Trade)trade).getStock()) &&
               getTradeType() == ((Trade)trade).getTradeType() &&
               getPrice() == ((Trade)trade).getPrice() &&
               getSize() == ((Trade)trade).getSize() &&
               getTradeState() == ((Trade)trade).getTradeState() &&
               getLastStateChange().equals(((Trade)trade).getLastStateChange()) && 
               getStrategyId()== ((Trade)trade).getStrategyId();
    }

    public static String toXml(Trade trade) {
        StringWriter stringWriter = new StringWriter();

        try {
            marshaller.marshal(trade, stringWriter);
            return stringWriter.toString();
        } catch(JAXBException e) {
            log.error("Unabled to marshall trade: " + trade);
            log.error("JAXBException: " + e);
        }
        return null;
    }

    public static Trade fromXml(String xmlString) {
        try {
            return (Trade)unmarshaller.unmarshal(new StringReader(xmlString));
        } catch (JAXBException e) {
            log.error("Unable to unmarshal trade: " + xmlString);
            log.error("JAXBException: " + e);
        }
        return null;
    }

	public int getStrategyId() {
		return strategyId;
	}

	public void setStrategyId(int strategyId) {
		this.strategyId = strategyId;
	}
}
