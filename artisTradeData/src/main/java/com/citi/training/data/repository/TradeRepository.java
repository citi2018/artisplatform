package com.citi.training.data.repository;

import java.util.List;

import com.citi.training.data.entities.Trade;

public interface TradeRepository {

    public Trade saveTrade(Trade Trade);

    public Trade getTradeById(int Id);

    public List<Trade> getTradesByStock(String stock);

    public List<Trade> getTradesByState(Trade.TradeState state);
    public List<Trade> getTradesByStrategyID(int id );
    public Trade getLatestTrade();

    public List<Trade> getAllTrades();

}
