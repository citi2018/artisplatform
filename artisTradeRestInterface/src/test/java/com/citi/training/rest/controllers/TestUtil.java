package com.citi.training.rest.controllers;

import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.data.entities.Trade;
import com.citi.training.data.repository.InMemTradeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class TestUtil {

    private static final Logger log = LoggerFactory
            .getLogger(InMemTradeRepository.class);

    public static Trade testTrade = new Trade();
    public static List<Trade> tradeList = Arrays.asList(testTrade);

    // localdatetime format here is not exactly the same as for REST interface
    // It suffices for testing
    public static ObjectMapper objMapper = new ObjectMapper().registerModule(new JavaTimeModule());

    public static String testJson() throws JsonProcessingException {
        return objMapper.writeValueAsString(testTrade);
    }

    public static void assertUnexpectedResult(String msg,
                                              Trade trade,
                                              MvcResult result)
                                              throws UnsupportedEncodingException {
        assertUnexpectedResult(msg, trade, result.getResponse().getContentAsString());
    }

    public static void assertUnexpectedResult(String msg,
                                              Trade trade,
                                              String result)
                                              throws UnsupportedEncodingException {
        log.info("Received from REST: " + result);
        assertTrue(msg + ": " + trade, result.contains(trade.getStock())
                && result.contains(trade.getTradeType().toString()));
    }
}
