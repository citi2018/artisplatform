package com.citi.training.rest.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.data.entities.Trade;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;


/* Tests full stack, but doesn't create servlet/web server */
/* These tests might access database */

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MockMVCTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void test_createTrade_sanityCheck() throws Exception {
        MvcResult result = this.mockMvc
                .perform(post("/trades")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.testJson()))
                .andExpect(status().isCreated()).andReturn();

        TestUtil.assertUnexpectedResult("createTrade expected",
                                        TestUtil.testTrade, result);
    }

    @Test
    public void test_getAll_sanityCheck() throws Exception {
        test_createTrade_sanityCheck();

        MvcResult result = this.mockMvc.perform(get("/trades"))
                                       .andExpect(status().isOk()).andReturn();
        TestUtil.assertUnexpectedResult("getAll expected", TestUtil.testTrade,
                                        result);
    }

    @Test
    public void test_getByStock_sanityCheck() throws Exception {
        test_createTrade_sanityCheck();

        MvcResult result = this.mockMvc
                .perform(get("/trades/" + TestUtil.testTrade.getStock()))
                .andExpect(status().isOk()).andReturn();
        TestUtil.assertUnexpectedResult("getByStock expected", TestUtil.testTrade,
                                        result);
    }

    @Test
    public void test_getById_sanityCheck() throws Exception {
        MvcResult result = this.mockMvc
                .perform(post("/trades")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.testJson()))
                .andExpect(status().isCreated()).andReturn();

        Trade newTrade = (new ObjectMapper().registerModule(new JavaTimeModule())).readValue(
                                    result.getResponse().getContentAsString(),
                                    Trade.class);

        result = this.mockMvc
                .perform(get("/trades/" + newTrade.getId()))
                .andExpect(status().isOk()).andReturn();
        TestUtil.assertUnexpectedResult("getById expected", TestUtil.testTrade,
                                        result);
    }
}
