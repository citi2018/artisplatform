package com.citi.training.rest.controllers;

import java.io.Console;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.data.entities.Strategies;
import com.citi.training.data.entities.Strategy;
import com.citi.training.data.entities.Trade;
import com.citi.training.data.services.StrategyService;
import com.citi.training.data.services.TradeService;

@RestController
@RequestMapping("/strategy")
public class StrategyRestController {

    public static final Logger log = LoggerFactory.getLogger(
                                            StrategyRestController.class);

    @Autowired
    private StrategyService strategyService;

   @RequestMapping(value = "/save", method = RequestMethod.POST)
	public Strategy save(@RequestBody Strategy strategy) 
	{
		return strategyService.saveStrategy(strategy);	
	}
   
//   @RequestMapping(value = "/list", method = RequestMethod.POST)
//  	public List<Strategy> list(@RequestBody Strategy strategy) 
//  	{
//  		return strategyService.list();	
//  	}
   
   @RequestMapping(value = "/status", method = RequestMethod.GET)
	public String getStatus()
	{
		return "RunningOK";
	}
   
   @RequestMapping(value = "/listall", method = RequestMethod.GET)
   public List<Strategies> list() 
 	{
 		return strategyService.listStrategy();
 	}
  
 
//
//    @RequestMapping(method = RequestMethod.POST,
//    		value = "/create",
//                    consumes = {"application/json"})
//    @ResponseStatus(HttpStatus.CREATED)
//    public Trade createTrade(@RequestBody Trade newTrade) {
//        log.debug("REST Controller rxd trade:\n" + newTrade);
//        return tradeService.saveTrade(newTrade);
//    }
//
//    @RequestMapping(method = RequestMethod.GET,
//                    value = "/{id:[0-9]*}",
//                    produces = {"application/json"})
//    public Trade getById(@PathVariable int id) {
//        log.debug("REST Controller getById: " + id);
//        return tradeService.getTradeById(id);
//    }
//
//    @RequestMapping(method = RequestMethod.GET,
//                    value = "/{stock:[A-z]*}",
//                    produces = {"application/json"})
//    public List<Trade> getByStock(@PathVariable String stock) {
//        return tradeService.getTradesByStock(stock);
//    }
//
//    @RequestMapping(method = RequestMethod.GET,
//            value = "/state/{state}",
//            produces = {"application/json"})
//    public List<Trade> getByState(@PathVariable Trade.TradeState state) {
//        return tradeService.getTradesByState(state);
//    }
}
