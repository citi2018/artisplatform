package com.citi.training.rest.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.data.entities.Holdings;
import com.citi.training.data.entities.Trade;
import com.citi.training.data.services.HoldingsService;

@RestController
@RequestMapping("/holdings")
public class HoldingsRestController {
    public static final Logger log = LoggerFactory.getLogger(HoldingsRestController.class);
    
    @Autowired
    private HoldingsService holdingsService;
    
    @RequestMapping(method = RequestMethod.GET,
			value="/listall",
            produces = {"application/json"})
    public List<Holdings> getAll(){
    	return holdingsService.getAllHoldings();
    }
    
    @RequestMapping(method = RequestMethod.GET,
            value = "/strategy/{strategy:[0-9]*}",
            produces = {"application/json"})
    public List<Holdings> getByStrategy(@PathVariable Integer strategy) {
        return holdingsService.getHoldingsByStrategyID(strategy);
    }

}
