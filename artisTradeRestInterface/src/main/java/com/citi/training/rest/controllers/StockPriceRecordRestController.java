package com.citi.training.rest.controllers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.data.entities.StockPriceRecord;
import com.citi.training.data.services.StockPriceHistoryService;

@RestController
@RequestMapping("/pricerecords")
public class StockPriceRecordRestController {

	public static final Logger log = LogManager.getLogger(StockPriceRecordRestController.class);

	@Autowired
	private StockPriceHistoryService stockPriceHistory;

	@RequestMapping(value = "/listall", method = RequestMethod.GET)
	public List<StockPriceRecord> list() {
		return stockPriceHistory.getAllRecords();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save", consumes = { "application/json" })
	@ResponseStatus(HttpStatus.CREATED)
	public StockPriceRecord saveStockPrice(@RequestBody StockPriceRecord newRecord) {
		log.debug("REST Controller rxd record:\n" + newRecord);
		return stockPriceHistory.saveRecord(newRecord);
	}
}
