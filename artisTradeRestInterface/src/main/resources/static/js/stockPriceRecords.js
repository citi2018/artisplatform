/**
 * 
 */

$(document).ready(startup);

function startup() {
	getStockPriceRecords()
}
function getStockPriceRecords() {
	$
			.ajax({
				url : "/pricerecords/listall",
				dataType : "json",
				type : "GET",
				cache : false,
				async : false,
				contentType : 'application/json; charset=utf-8',
				success : function(response) {
					var output = "<Table><thead><tr><th>Id</th><th>Ticker</th><th>Time Recorded</th><th>Price</th></tr></thead>";
					for (var i = 0; i < response.length; i++) {
						output += "<tr  class='ui-widget-content'>"
						output += "<td width='200'>" + response[i].id + "</td>";
						output += "<td width='200'>" + response[i].ticker
								+ "</td>";
						output += "<td width='200'>"+ response[i].timeInspected 
								+ "</td>";
						output += "<td width='200'>" + response[i].price
								+ "</td>";
						output += "</tr>"
					}
					output += "</Table> ";
					$("#panelResults").html(output);
				},
				error : function(err) {
					alert("Error: " + err.responseText)
				}
			});
}