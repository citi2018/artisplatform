/**
 * 
 */
$(document).ready(startup);

function startup() {
	getHoldings()
}
function getHoldings() {
	$
			.ajax({
				url : "/holdings/listall",
				dataType : "json",
				type : "GET",
				cache : false,
				async : false,
				contentType : 'application/json; charset=utf-8',
				success : function(response) {
					var output = "<Table><thead><tr><th>Stock Name</th><th>Holdings</th><th>Cash Balance</th></tr></thead>";
					for (var i = 0; i < response.length; i++) {
						output += "<tr  class='ui-widget-content'>"
						output += "<td width='200'>" + response[i].stockName
								+ "</td>";
						output += "<td width='200'>" + response[i].holdings
								+ "</td>";
						output += "<td width='200'>" + response[i].cashBalance
								+ "</td>";
						output += "</tr>"
					}
					output += "</Table> ";
					$("#panelResults").html(output);
				},
				error : function(err) {
					alert("Error: " + err.responseText)
				}
			});
}