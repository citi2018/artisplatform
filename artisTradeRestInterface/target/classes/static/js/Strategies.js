/**
 *
 */

$(document).ready(startup);

function startup()
{
	getStrategies()
    
}
setInterval(getTrades, 5000);

function getStrategies()
{

	$.ajax({
        url: "/strategy/listall",
        dataType: "json",
        type: "GET",
        cache: false,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
        		// alert(response[1].contactname);
        			var output = "<table class='table table-striped'><thead><tr><th>ID</th><th>Active</th><th>Name</th><th>Close</th><th>EntrySize</th><th>Ticker</th><th>Profit</th></tr></thead>";
        			for (var i=0; i<response.length; i++){
        				output+="<tr  class='ui-widget-content'>"
            			output+="<td width='200'>" + response[i].id + "</td>";
        				output+="<td width='200'>" + response[i].isActive + "</td>";
        				output+="<td width='200'>" + response[i].name + "</td>";
        				output+="<td width='200'>" + response[i].closePercentage + "</td>";
        				output+="<td width='200'>" + response[i].entrySize + "</td>";
        				output+="<td width='200'>" + response[i].ticker + "</td>";
        				output+="<td width='200'>" + response[i].profitValue + "</td>";
                		output+="</tr>"


        			}
        			output+="</Table> ";
            			$("#panelResults").html(output);
      },
        error: function (err) {
            alert("Error: " + err.responseText)
        }
    });
}
function getTrades()
{

	$.ajax({
        url: "/trades/strategy/7",
        dataType: "json",
        type: "GET",
        cache: false,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
        		// alert(response[1].contactname);
        			var output = "<Table class='table'><thead><tr><th>ID</th><th>Stock</th><th>Price</th><th>Size</th><th>LastStateChange</th><th>TradeType</th><th>TradeState</th></tr></thead>";
        			for (var i=0; i<response.length; i++){
        				output+="<tr  class='ui-widget-content'>"
            			output+="<td width='200'>" + response[i].id + "</td>";
        				output+="<td width='200'>" + response[i].stock + "</td>";
        				output+="<td width='200'>" + response[i].price + "</td>";
        				output+="<td width='200'>" + response[i].size + "</td>";
        				output+="<td width='200'>" + response[i].lastStateChange + "</td>";
        				output+="<td width='200'>" + response[i].tradeType + "</td>";
        				output+="<td width='200'>" + response[i].tradeState + "</td>";
                		output+="</tr>"
        			}
        			output+="</Table> ";
            			$("#tradeResults").html(output);
      },
        error: function (err) {
            alert("Error: " + err.responseText)
        }
    });
}
