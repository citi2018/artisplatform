$(document).ready(startup);

function startup() {

	// hookupevents
	$("#submit").click(save);
}


$('#submit').click(function (e) {
    var isValid = true;
    $('#txtActive,#txtStrategy,#txtClosePercentage,#txtTicker').each(function () {
        if ($.trim($(this).val()) == '') {
            isValid = false;
            $(this).css({
                "border": "1px solid red",
                "background": "#FFCECE"
            });
        }
        else {
            $(this).css({
                "border": "",
                "background": ""
            });
        }
    });
    if (isValid == false)
        e.preventDefault();

})

function myFunctionSuccess() {
    var x = document.getElementById("success");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

function myFunctionError() {
    var x = document.getElementById("error");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

function save() {

	var id = 0;
	var isActive = $("#txtAmount").val();
	var name = $("#txtStrategy").val();
	var closePercentage = $("#txtClosePercentage").val();
	var entrySize = $("#txtEntrySize").val();
	var ticker = $("#txtTicker").val();
	var profitValue = $("#txtProfit").val();

	var params = {
		id:0,
		isActive:isActive,
		name:name,
		closePercentage:closePercentage,
		entrySize:entrySize,
		ticker:ticker,
		profitValue:0,
	};

	var paramsJson = JSON.stringify(params);

	$.ajax({
		url : "/strategy/save",
		data : paramsJson,
		dataType : "json",
		type : "POST",
		cache : false,
		async : false,
		contentType: 'application/json; charset=utf-8',
		success : function(response) {
			myFunctionSuccess();
		},
		error : function(err) {
			myFunctionError();
		}
	});
	
}

