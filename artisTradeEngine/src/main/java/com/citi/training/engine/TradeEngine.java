package com.citi.training.engine;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.data.entities.DoubleAverage;
import com.citi.training.data.entities.Holdings;
import com.citi.training.data.entities.StockPriceRecord;
import com.citi.training.data.entities.Strategies;
import com.citi.training.data.entities.Trade;
import com.citi.training.data.entities.Trade.TradeState;
import com.citi.training.data.entities.Trade.TradeType;
import com.citi.training.data.repository.JdbcHoldingsRepository;
import com.citi.training.data.repository.JdbcStockPriceRecordRepository;
import com.citi.training.data.repository.JdbcTradeRepository;
import com.citi.training.data.services.HoldingsService;
import com.citi.training.data.services.StockPriceHistoryService;
import com.citi.training.data.services.StrategyService;
import com.citi.training.data.services.TradeService;

@Component
public class TradeEngine {

	private static final Logger log = LogManager.getLogger(TradeEngine.class);

	@Autowired
	private TradeService tradeService;
	@Autowired
	private PriceFeed priceFeed;
	@Autowired
	private StrategyService strategyService;
	@Autowired
	private HoldingsService holdingService;
	@Autowired
	private StockPriceHistoryService stockPriceService;
	@Autowired
	private JdbcHoldingsRepository holdingRepository;
	@Autowired
	private TradeSender ts;
	@Autowired
	private JdbcTradeRepository tradeRepository;

	@Autowired
	private JdbcStockPriceRecordRepository stockPriceRepository;

	Strategies strats = new Strategies();

	@Scheduled(fixedRate = 5000)

	public void createTrades() throws ParseException {
		// get price fron yahoo

		// find the average price of the records for boSC th the long and short periods

		Trade trade = null;
		StockPriceRecord stockPrice = null;
		String stock = "";
		float price = 0;
		int size = 0;
		LocalDateTime lastStateChange = LocalDateTime.now();
		TradeType tradeType = TradeType.SELL;
		TradeState state = TradeState.INIT;
		int strategyId = 0;
		Trade latest = tradeService.getLatestTrades();

		List<Strategies> strategies = strategyService.getActiveStrategies();
		for (Strategies strategy : strategies) {
			strategyId = strategy.getId();
			stock = strategy.getTicker();
			size = strategy.getEntrySize();
		}

		price = priceFeed.liveFeed(stock);
		stockPrice = new StockPriceRecord(stock, price);
		stockPriceRepository.saveRecord(stockPrice);

		Float shortAvg = stockPriceService.getShortAverage(stock);
		System.out.println(shortAvg);
		Float longAvg = stockPriceService.getLongAverage(stock);
		System.out.println(longAvg);
		System.out.println(shortAvg - longAvg);
		System.out.println(latest.getTradeType());
		
		if (((shortAvg > longAvg)) && (latest.getTradeType().equals(TradeType.BUY))) {
			tradeType = TradeType.SELL;
			price = priceFeed.liveFeed(stock);
			trade = new Trade(stock, price, size, lastStateChange, tradeType, state, strategyId);
			sendTrades(trade);
			tradeRepository.saveTrade(trade);
			log.info("Trade created as a sell");
			
		} else if (((shortAvg < longAvg)) && (latest.getTradeType().equals(TradeType.SELL))) {
			tradeType = TradeType.BUY;
			price = priceFeed.liveFeed(stock);
			trade = new Trade(stock, price, size, lastStateChange, tradeType, state, strategyId);
			sendTrades(trade);
			tradeRepository.saveTrade(trade);
			log.info("Trade created as a buy");
			
		} else if ((shortAvg - longAvg) == 0) {
			System.out.println("Equal");
		} else {
			strats.setIsActive(false);
		}
	
	}

	public void sendTrades(Trade trade) {

		String message = Trade.toXml(trade);
		System.out.println(message);
		log.info("Trade sent" + message);
		ts.sendSimple(message, trade.getId());
		trade.stateChange(Trade.TradeState.FILLED);
		tradeService.saveTrade(trade);
	}
}// sendTrades
