package com.citi.training.engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class TradeSender {

	@Autowired
	private JmsTemplate jmsTemplate;

	public void sendSimple(String trade, int tradeid)
	{
		jmsTemplate.convertAndSend("OrderBroker", trade,
			message -> {
				message.setStringProperty("Operation", "Update");
				// parse int to string
				message.setJMSCorrelationID(String.valueOf(tradeid));
				return message;
			});
	}
}
