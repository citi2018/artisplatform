package com.citi.training.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.citi.training.data.entities.DoubleAverage;
import com.citi.training.data.entities.Trade;
import com.citi.training.data.entities.Trade.TradeType;
//import com.citi.training.data.entities.TradeTest;
import com.citi.training.data.services.TradeService;
import com.fasterxml.jackson.databind.util.EnumValues;

public class TradeEngineDoubleAverage {

	@Autowired
	private DoubleAverage strategy;
	@Autowired
	private Trade trade;
	@Autowired
	private PriceFeed pricefeed;
	@Autowired
	private 


	double startingValue = 0;
	int tradeSize = strategy.getEntrySize();
	double exitPercent = strategy.getClosePercentage();
	double check = 0;
	// running pnl since start of strategy
	double pnl = 0;
	// a constantly updated pnl of a buy and sell pair
	double pairPnl = 0;
	double prevLongAvg = 0, prevShortAvg = 0;
	// shows if the current iteration of the loop has to send a close order
	boolean hasToClose = false;
	{
			int longTime = strategy.getLongTime();
			int shortTime = strategy.getShortTime();

			// find the average price of the records for boSC th the long and short periods
			Float longAvg = pricefeed.getAverageStockPrice();
			Float shortAvg =pricefeed.getAverageStockPrice();
			if (longAvg == null || shortAvg == null) {
				// in case the feed doesn't have any data on prices yet

			float currentPrice = pricefeed.liveFeed(strategy.getTicker());
			if (startingValue == 0) {
				startingValue = currentPrice * tradeSize;
				check = startingValue * (exitPercent / 100);
			}

			Trade order = null;

			if (prevLongAvg != 0 && prevShortAvg != 0) {

				if ((prevLongAvg - prevShortAvg) < 0 && (longAvg - shortAvg) > 0) {
					// you should sell
					//order = new Trade();
					pairPnl += currentPrice * tradeSize;
				} else if ((prevLongAvg - prevShortAvg) > 0 && (longAvg - shortAvg) < 0) {
					// you should buy
					//order = new Trade(strategy.getTicker(), currentPrice, tradeSize, TradeType.BUY);
					pairPnl -= currentPrice * tradeSize;
				}
			}

			prevLongAvg = longAvg;
			prevShortAvg = shortAvg;

			strategy.setIsActive(false);
			strategy.setProfitValue(pnl);
		}
	}
}

