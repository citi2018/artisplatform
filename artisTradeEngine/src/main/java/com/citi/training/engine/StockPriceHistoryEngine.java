package com.citi.training.engine;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.data.entities.StockPriceRecord;
import com.citi.training.data.services.StockPriceHistoryService;


@Component
public class StockPriceHistoryEngine {
	private static final Logger log = LoggerFactory.getLogger(TradeEngine.class);
	
	@Autowired
	StockPriceHistoryService stockPrice;
	@Autowired
	PriceFeed priceFeed;
	
	
//	public void createRecords() {
//		StockPriceRecord stockRecord = null;
//		String ticker = "";
//		LocalDateTime timeInspected = LocalDateTime.now();
//		float price = 0;
//	}
	public float getStockPrice(String stock) {
		log.debug("Obtaining new price from live feed for stock: " + stock);
		return priceFeed.liveFeed(stock);
	}
}
