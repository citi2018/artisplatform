package com.citi.training.engine;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class PriceFeed {
	 private ArrayList<String> onlineFeed = new ArrayList<String>();
	 public ArrayList<Float> stockPrices = new ArrayList<Float>(); 
	 
	public float liveFeed(String stockName){
		RestTemplate restTemplate = new RestTemplate();
		String url  = "http://feed.conygre.com:8080/MockYahoo/quotes.csv?s="
				+ stockName + "&f=p0";
		ResponseEntity<String> response
		  = restTemplate.getForEntity(url, String.class);
		onlineFeed.add(response.getBody());
		int feedLength = onlineFeed.size()-1;
		float price = Float.parseFloat(onlineFeed.get(feedLength));
		System.out.println(price);
		stockPrices.add(price);
		return price;
			}//liveFeed
	
	public float getAverageStockPrice() {
		float total =0;
		float average = 0;
		for(int i = 0; i < stockPrices.size(); i ++) {
			total = total + stockPrices.get(i);
		}
		average = total/stockPrices.size();
	return average;	
	}
		}// PriceFeed
