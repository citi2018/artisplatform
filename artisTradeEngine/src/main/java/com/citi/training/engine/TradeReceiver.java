package com.citi.training.engine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class TradeReceiver {

	private static final Logger log = LogManager.getLogger(TradeReceiver.class);
	
	@JmsListener(destination="OrderBroker_Reply")
	public void receive(String message) {
		System.out.println("RECEIVED " +message);
		log.info("RECEIVED " +message);
		// look up demo for correlation id
	}
}
