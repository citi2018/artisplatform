package com.citi.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.citi.training.engine.TradeSender;

@SpringBootApplication
@EnableScheduling
public class TradeEngineApplication {

    public static void main(String[] args) {
    	SpringApplication.run(TradeEngineApplication.class, args);
        
    }
}
