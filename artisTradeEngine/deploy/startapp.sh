#!/bin/bash

# Variables from the environment, if they are not set then use these values
if [[ -z DBSERVER ]]
then
  DBSERVER=localhost
fi
if [[ -z DBNAME ]]
then
  DNAME=myapp
fi
if [[ -z DBUSER ]]
then
  DBUSER=mydbusername
fi
if [[ -z DBPASS ]]
then
  DBPASS=mydbuserpassword
fi

# Create the properties file
cat >application.properties <<_END_

spring.datasource.url=jdbc:mysql://${DBSERVER}:3306/${DBNAME}?useSSL=false
spring.datasource.username=${DBUSER}
spring.datasource.password=${DBPASS}
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.activemq.broker-url=tcp://localhost:61616
spring.activemq.user=admin
spring.activemq.password=admin


_END_

# Start the application

java -jar artisTradeEngine-0.0.1-SNAPSHOT.jar
